/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author muhremantra
 */
public class Waktu 
{
    public static int DEFAULT_HARI = 0;
    public static int GMT_0 = 0;
    public static int DETIK_DALAM_SATU_HARI = 86400;
    
    public Waktu() 
    { 
        this.resetWaktu();
    }
    
    public Waktu(int hari, int jam, int menit, int detik, int zonaWaktu)
    {
        this.setHari(hari);
        this.setJam(jam);
        this.setMenit(menit);
        this.setDetik(detik);
        this.setZonaWaktu(zonaWaktu);
    }
    
    public Waktu(String jam, String menit, String detik, String zonaWaktu)
    {
        this(Integer.parseInt(jam), Integer.parseInt(menit), Integer.parseInt(detik), Integer.parseInt(zonaWaktu));
    }
    
    public Waktu(int jam, int menit, int detik, int zonaWaktu)
    {
        this(DEFAULT_HARI, jam, menit, detik, zonaWaktu);
    }
    
    public Waktu(int jam, int menit, int detik)
    {
        this(jam, menit, detik, GMT_0);
    }
    
    public void resetWaktu()
    {
        this.setHari(0);
        this.setJam(0);
        this.setMenit(0);
        this.setDetik(0);
        this.setZonaWaktu(0);
    }
    
    public boolean validasiWaktu()
    {
        return (0 <= this.getJam() && this.getJam() <= 23)
                && (0 <= this.getMenit() && this.getMenit() <= 59)
                && (0 <= this.getDetik() && this.getDetik() <= 59)
                && (-12 <= this.getZonaWaktu() && this.getZonaWaktu() <= 14);
    }
    
    public int konversiWaktuKeDetik()
    {
        return (((this.getHari() * 24) + this.getJam() - this.getZonaWaktu()) * 3600) 
            + (this.getMenit() * 60)
            + (this.getDetik());
    }
    
    public Waktu konversiDetikKeWaktu(int detik)
    {
        int newMenit = detik / 60;
        int detikTersisa = detik - (newMenit * 60);
        int newJam = newMenit / 60;
        int menitTersisa = newMenit - (newJam * 60);
        int newHari = newJam / 24;
        int jamTersisa = newJam - (newHari * 24);
        
        return new Waktu(newHari, jamTersisa, menitTersisa, detikTersisa, GMT_0);
    }
                
    public Waktu hitungSelisihWaktu(Waktu waktuLain)
    {
        int detikPertama = this.konversiWaktuKeDetik();
        int detikKedua = waktuLain.konversiWaktuKeDetik();
        int selisihDetik = detikKedua - detikPertama;
        System.out.println(detikPertama);
        System.out.println(detikKedua);
        System.out.println(selisihDetik);
        
        int jumlahHariBerlalu = 0;
        
        while (selisihDetik < 0)
        {
            selisihDetik += DETIK_DALAM_SATU_HARI;
            if (selisihDetik < 0)
            {
                ++jumlahHariBerlalu;
            }
        }
        
        Waktu selisihWaktu = this.konversiDetikKeWaktu((jumlahHariBerlalu * DETIK_DALAM_SATU_HARI) + selisihDetik);
        selisihWaktu.setHari(jumlahHariBerlalu);
        return selisihWaktu;
    }
    
    private int normalkanDetik(int detik)
    {
        return (detik < 0) ? detik + DETIK_DALAM_SATU_HARI : detik;
    }
    
    public String printWaktu()
    {
        return "" 
                + this.getHari() + " Hari " 
                + this.getJam() + " Jam " 
                + this.getMenit() + " Menit " 
                + this.getDetik() + " Detik ";  
//                + "GMT" + (this.getZonaWaktu() >= 0 ? "+" : "") + this.getZonaWaktu());
    }    
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) 
    {
        Waktu w1 = new Waktu(0, 0, 0, -12);
        w1.printWaktu();
        System.out.println("is w1 valid? " + w1.validasiWaktu());
        Waktu w2 = new Waktu(23, 59, 59, 14);
        w2.printWaktu();
        System.out.println("is w2 valid? " + w2.validasiWaktu());
        Waktu selisihWaktu = w2.hitungSelisihWaktu(w1);
        System.out.println(selisihWaktu.printWaktu());
    }
    
    // Getters and Setters
    int hari; // sebagai buffer untuk jam yang > 24 jam 
    int jam;
    int menit;
    int detik;
    int zonaWaktu; // dalam GMT
    
    public int getHari()
    {
        return this.hari;
    }
    
    public void setHari(int newHari)
    {
        this.hari = newHari;
    }    
    
    public int getJam()
    {
        return this.jam;
    }
    
    public void setJam(int newJam)
    {
        this.jam = newJam;
    }
    
    public int getMenit()
    {
        return this.menit;
    }
    
    public void setMenit(int newMenit)
    {
        this.menit = newMenit;
    }
    
    public int getDetik()
    {
        return this.detik;
    }
    
    public void setDetik(int newDetik)
    {
        this.detik = newDetik;
    }
    
    public int getZonaWaktu()
    {
        return this.zonaWaktu;
    }
    
    public void setZonaWaktu(int newZonaWaktu)
    {
        this.zonaWaktu = newZonaWaktu;
    }
}
