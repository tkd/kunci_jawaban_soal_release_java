
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author muhremantra
 */
public class JajarGenjangGenerator 
{
    public static int MODE_1 = 1;
    public static int MODE_2 = 2;
    public boolean isInputValid(int sisiSejajar, int lebar)
    {
        return (sisiSejajar > 1) && (lebar > 1);
    }
    
    public void print(int sisiSejajar, int lebar, int mode)
    {
        if (mode == MODE_1)
        {
            this.printMode1(sisiSejajar, lebar);
        }
        else if (mode == MODE_2)
        {
            this.printMode2(sisiSejajar, lebar);
        }
        else
        {
            System.out.println("mode tidak valid");
        }
    }
    
    public void printMode1(int sisiSejajar, int lebar)
    {
        printSpasiLuar(lebar-1);
        printSisiSejajar(sisiSejajar);
        for (int i = lebar-2; i > 0; --i)
        {
            printSpasiDalam(i, sisiSejajar);
        }
        printSisiSejajar(sisiSejajar);  
    }
    
    public void printMode2(int sisiSejajar, int lebar)
    {
        printSisiSejajar(sisiSejajar);  
        for (int i = 1; i < lebar-1; ++i)
        {
            printSpasiDalam(i, sisiSejajar);
        }
        printSpasiLuar(lebar-1);
        printSisiSejajar(sisiSejajar);
    }
    
    public void printSisiSejajar(int sisiSejajar)
    {
        for (int i = 0; i < sisiSejajar; ++i)
        {
            System.out.print("#");
        }
        
        System.out.println("");
    }
    
    public void printSpasiLuar(int iterator)
    {
        for (int i = 0; i < iterator; ++i)
        {
            System.out.print(" ");
        }
    }

    public void printSpasiDalam(int iterator, int sisiSejajar)
    {
        this.printSpasiLuar(iterator);
        System.out.print("#");
        this.printSpasiLuar(sisiSejajar-2);
        System.out.println("#");
    }

    public static void main(String args[])
    {
        boolean isExit = false;
        JajarGenjangGenerator jGen = new JajarGenjangGenerator();
        Scanner s = new Scanner(System.in);
        while (!isExit)
        {
            System.out.print("input sisi sejajar: ");
            int sisiSejajar = s.nextInt();
            System.out.print("input lebar: ");
            int lebar = s.nextInt();
            if (!jGen.isInputValid(sisiSejajar, lebar))
            {
                System.out.println("ukuran jajar genjang minimum 2x2");
            }
            else
            {                 
                System.out.print("input mode [1/2]: ");
                int mode = s.nextInt();
                jGen.print(sisiSejajar, lebar, mode);
                
                System.out.print("ingin melanjutkan [y/n]: ");
                String lanjut = s.next();
                if (lanjut.equalsIgnoreCase("y") || (lanjut.equalsIgnoreCase("n")))
                {
                    isExit = lanjut.equalsIgnoreCase("n");
                }
                else
                {
                    System.out.println("input tidak valid");
                }
            }
        }
                    
        System.out.println("aplikasi berhasil ditutup");
    }
}