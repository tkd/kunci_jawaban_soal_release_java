
import java.awt.CardLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JComboBox;
import javax.swing.JLayeredPane;
import javax.swing.JPanel;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author muhremantra
 */
public class BidangFrame extends javax.swing.JFrame 
{
    private static final String BIDANG_PERSEGI = "Persegi";
    private static final String BIDANG_SEGITIGA = "Segitiga";
    private static final String BIDANG_LINGKARAN = "Lingkaran";
    private static final String BIDANG_JAJARGENJANG = "Jajar Genjang";
    
    /**
     * Creates new form BidangJFrame
     */
    public BidangFrame() {
        this.initComponents();
        this.initUI();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pilihanBidangComboBox = new javax.swing.JComboBox<>();
        pilihanBidangLayeredPane = new javax.swing.JLayeredPane();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBounds(new java.awt.Rectangle(0, 0, 800, 480));
        setPreferredSize(new java.awt.Dimension(800, 480));

        pilihanBidangComboBox.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Persegi", "Segitiga", "Lingkaran", "Jajar Genjang" }));
        pilihanBidangComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pilihanBidangComboBoxActionPerformed(evt);
            }
        });

        pilihanBidangLayeredPane.setBackground(new java.awt.Color(205, 206, 207));
        pilihanBidangLayeredPane.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        pilihanBidangLayeredPane.setForeground(new java.awt.Color(161, 161, 161));
        pilihanBidangLayeredPane.setLayout(new java.awt.CardLayout());

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(pilihanBidangLayeredPane)
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addGap(100, 100, 100)
                .addComponent(pilihanBidangComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 547, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(100, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(pilihanBidangComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pilihanBidangLayeredPane, javax.swing.GroupLayout.DEFAULT_SIZE, 250, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void pilihanBidangComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pilihanBidangComboBoxActionPerformed
        // TODO add your handling code here:
        // reset all inputs
        this.getPersegiPanel().resetInputs();
        this.getPersegiPanel().setPlaceholder(this.getPersegiPanel().getR2Input(), "sisi");
        this.getSegitigaPanel().resetInputs();
        this.getSegitigaPanel().setPlaceholder(this.getSegitigaPanel().getR1Input(), "alas");
        this.getSegitigaPanel().setPlaceholder(this.getSegitigaPanel().getR2Input(), "tinggi");
        this.getLingkaranPanel().resetInputs();
        this.getLingkaranPanel().setPlaceholder(this.getLingkaranPanel().getR2Input(), "jari-jari");
        this.getJajarGenjangPanel().resetInputs();
        this.getJajarGenjangPanel().setPlaceholder(this.getJajarGenjangPanel().getR1Input(), "sisi sejajar");
        this.getJajarGenjangPanel().setPlaceholder(this.getJajarGenjangPanel().getR2Input(), "lebar");
        
        String pilihanBidang = (String)((JComboBox)evt.getSource()).getSelectedItem();
        CardLayout pilihanBidangCardLayout = (CardLayout)(this.getPilihanBidangPanel().getLayout());
        pilihanBidangCardLayout.show(this.getPilihanBidangPanel(), pilihanBidang);
    }//GEN-LAST:event_pilihanBidangComboBoxActionPerformed

    public void initUI()
    {
        this.setLocationRelativeTo(null);
        this.initPersegiPanel();
        this.initSegitigaPanel();
        this.initLingkaranPanel();
        this.initJajarGenjangPanel();
        
        this.getPilihanBidangPanel().add(this.getPersegiPanel(), BIDANG_PERSEGI);
        this.getPilihanBidangPanel().add(this.getSegitigaPanel(), BIDANG_SEGITIGA);
        this.getPilihanBidangPanel().add(this.getLingkaranPanel(), BIDANG_LINGKARAN);
        this.getPilihanBidangPanel().add(this.getJajarGenjangPanel(), BIDANG_JAJARGENJANG);
    }    
    
    public void initPersegiPanel()
    {
        this.setPersegiPanel(new BidangPanel(new Persegi()));
        this.getPersegiPanel().getR1Input().setVisible(false);
        this.getPersegiPanel().getR2Input().setVisible(true);
        this.getPersegiPanel().setPlaceholder(this.getPersegiPanel().getR2Input(), "sisi");
        this.getPersegiPanel().getHitungButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) 
            {
                float sisi = Float.parseFloat(getPersegiPanel().getR2Input().getText());
                float luasBidang = getPersegiPanel().getBidang().hitungLuas(sisi);
                String luas = String.format("%.2f", luasBidang);
                getPersegiPanel().getHasilText().setText("Luas: " + luas);                
            }
        });
    }
        
    public void initSegitigaPanel()
    {
        this.setSegitigaPanel(new BidangPanel(new Segitiga()));
        this.getSegitigaPanel().getR1Input().setVisible(true);
        this.getSegitigaPanel().getR2Input().setVisible(true);
        this.getSegitigaPanel().setPlaceholder(this.getSegitigaPanel().getR1Input(), "alas");
        this.getSegitigaPanel().setPlaceholder(this.getSegitigaPanel().getR2Input(), "tinggi");
        this.getSegitigaPanel().getHitungButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) 
            {
                float alas = Float.parseFloat(getSegitigaPanel().getR1Input().getText());
                float tinggi = Float.parseFloat(getSegitigaPanel().getR2Input().getText());
                float luasBidang = getSegitigaPanel().getBidang().hitungLuas(alas, tinggi);
                String luas = String.format("%.2f", luasBidang);
                getSegitigaPanel().getHasilText().setText("Luas: " + luas);                
            }
        });
    }       
    
    public void initLingkaranPanel()
    {
        this.setLingkaranPanel(new BidangPanel(new Lingkaran()));   
        this.getLingkaranPanel().getR1Input().setVisible(false);
        this.getLingkaranPanel().getR2Input().setVisible(true);
        this.getLingkaranPanel().setPlaceholder(this.getLingkaranPanel().getR2Input(), "jari-jari");
        this.getLingkaranPanel().getHitungButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) 
            {
                float jariJari = Float.parseFloat(getLingkaranPanel().getR2Input().getText());
                float luasBidang = getLingkaranPanel().getBidang().hitungLuas(jariJari);
                String luas = String.format("%.2f", luasBidang);
                getLingkaranPanel().getHasilText().setText("Luas: " + luas);                
            }
        });
    } 
        
    public void initJajarGenjangPanel()
    {
        this.setJajarGenjangPanel(new BidangPanel(new JajarGenjang()));   
        this.getJajarGenjangPanel().getR1Input().setVisible(true);
        this.getJajarGenjangPanel().getR2Input().setVisible(true);
        this.getJajarGenjangPanel().setPlaceholder(this.getJajarGenjangPanel().getR1Input(), "sisi sejajar");
        this.getJajarGenjangPanel().setPlaceholder(this.getJajarGenjangPanel().getR2Input(), "lebar");
        this.getJajarGenjangPanel().getHitungButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) 
            {
                float sisiSejajar = Float.parseFloat(getJajarGenjangPanel().getR1Input().getText());
                float lebar = Float.parseFloat(getJajarGenjangPanel().getR2Input().getText());
                float luasBidang = getJajarGenjangPanel().getBidang().hitungLuas(sisiSejajar, lebar);
                String luas = String.format("%.2f", luasBidang);
                getJajarGenjangPanel().getHasilText().setText("Luas: " + luas);                
            }
        });
    }
    
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(BidangFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(BidangFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(BidangFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(BidangFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new BidangFrame().setVisible(true);
            }
        });
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox<String> pilihanBidangComboBox;
    private javax.swing.JLayeredPane pilihanBidangLayeredPane;
    // End of variables declaration//GEN-END:variables

    private BidangPanel<Persegi> persegiPanel;
    private BidangPanel<Segitiga> segitigaPanel;
    private BidangPanel<Lingkaran> lingkaranPanel;
    private BidangPanel<JajarGenjang> jajarGenjangPanel;
    
    public JLayeredPane getPilihanBidangPanel()
    {
        return this.pilihanBidangLayeredPane;
    }
    
    public void setPilihanBidangPanel(JLayeredPane newPanel)
    {
        this.pilihanBidangLayeredPane = newPanel;
    }    
    
    public BidangPanel<Persegi> getPersegiPanel()
    {
        return this.persegiPanel;
    }
    
    public void setPersegiPanel(BidangPanel<Persegi> newPanel)
    {
        this.persegiPanel = newPanel;
    }
    
    public BidangPanel<Segitiga> getSegitigaPanel()
    {
        return this.segitigaPanel;
    }
    
    public void setSegitigaPanel(BidangPanel<Segitiga> newPanel)
    {
        this.segitigaPanel = newPanel;
    }
    
    public BidangPanel<Lingkaran> getLingkaranPanel()
    {
        return this.lingkaranPanel;
    }
    
    public void setLingkaranPanel(BidangPanel<Lingkaran> newPanel)
    {
        this.lingkaranPanel = newPanel;
    }
    
    public BidangPanel<JajarGenjang> getJajarGenjangPanel()
    {
        return this.jajarGenjangPanel;
    }
    
    public void setJajarGenjangPanel(BidangPanel<JajarGenjang> newPanel)
    {
        this.jajarGenjangPanel = newPanel;
    }
}
