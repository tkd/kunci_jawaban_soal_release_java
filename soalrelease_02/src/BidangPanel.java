
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author muhremantra
 * @param <T>
 */
public final class BidangPanel<T extends Bidang> extends javax.swing.JPanel 
{
    /**
     * Creates new form BidangPanel
     * @param bidang
     */
    public BidangPanel(T bidang) 
    {
        this.initComponents();
        this.initUI(bidang);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        r2Input = new javax.swing.JTextField();
        r1Input = new javax.swing.JTextField();
        hitungButton = new javax.swing.JButton();
        hasilText = new javax.swing.JLabel();

        r2Input.setText("r2");

        r1Input.setText("r1");

        hitungButton.setText("Hitung");

        hasilText.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        hasilText.setText("Luas: 0");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(86, 86, 86)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(hitungButton, javax.swing.GroupLayout.DEFAULT_SIZE, 238, Short.MAX_VALUE)
                    .addComponent(hasilText, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(r2Input)
                    .addComponent(r1Input, javax.swing.GroupLayout.Alignment.TRAILING))
                .addGap(76, 76, 76))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(72, 72, 72)
                .addComponent(r1Input, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(32, 32, 32)
                .addComponent(r2Input, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(29, 29, 29)
                .addComponent(hitungButton)
                .addGap(18, 18, 18)
                .addComponent(hasilText, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(33, 33, 33))
        );
    }// </editor-fold>//GEN-END:initComponents

    public void initUI(T bidang)
    {
        this.setBidang(bidang);
        this.getR1Input().setVisible(false);
        this.getR2Input().setVisible(false);
        this.resetInputs();
    }
    
    public void setPlaceholder(final JTextField textField, final String placeholder)
    {
        textField.setText(placeholder);
        textField.addFocusListener(new FocusListener()
        {
            @Override
            public void focusGained(FocusEvent fe) 
            {
                if (textField.getText().equals(placeholder)) 
                {
                   textField.setText("");
                }
            }

            @Override
            public void focusLost(FocusEvent fe) 
            {
                if (textField.getText().isEmpty()) 
                {
                   textField.setText(placeholder);
                }  
            }
        }); 
    }
    
    public void resetInputs()
    {
        this.getR1Input().setText("");
        this.getR2Input().setText("");   
        this.getHasilText().setText("Luas: 0");     
        
        this.setPlaceholder(this.getR1Input(), "r1");
        this.setPlaceholder(this.getR2Input(), "r2");
    }
    
    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel hasilText;
    private javax.swing.JButton hitungButton;
    private javax.swing.JTextField r1Input;
    private javax.swing.JTextField r2Input;
    // End of variables declaration//GEN-END:variables
    
    private T bidang;
    
    public T getBidang()
    {
        return this.bidang;
    }
    
    public void setBidang(T newBidang)
    {
        this.bidang = newBidang;
    }

    public JTextField getR1Input()
    {
         return this.r1Input;
    }
    
    public void setR1Input(JTextField newInput)
    {
        this.r1Input = newInput;
    }
    
    public JTextField getR2Input()
    {
        return this.r2Input;
    }
    
    public void setR2Input(JTextField newInput)
    {
        this.r2Input = newInput;
    }
    
    public JButton getHitungButton()
    {
        return this.hitungButton;
    }
    
    public void setHitungButton(JButton newButton)
    {
        this.hitungButton = newButton;
    }
    
    public JLabel getHasilText()
    {
        return this.hasilText;
    }
    
    public void setHasilText(JLabel newText)
    {
        this.hasilText = newText;
    }
}
