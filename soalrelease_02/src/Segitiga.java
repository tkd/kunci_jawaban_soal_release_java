/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author muhremantra
 */
public class Segitiga implements Bidang 
{
    @Override
    public float hitungLuas(float r1, float r2) 
    {
        return (r1 * r2) / 2;
    }    
}