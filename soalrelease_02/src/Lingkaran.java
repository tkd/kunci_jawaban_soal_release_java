/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author muhremantra
 */
public class Lingkaran implements Bidang 
{
    public float hitungLuas(float jariJari)
    {
        return this.hitungLuas(jariJari, jariJari);
    }

    @Override
    public float hitungLuas(float r1, float r2) 
    {
        return (float) (Math.PI * r1 * r2);
    }    
}