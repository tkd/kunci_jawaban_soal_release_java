
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author muhremantra
 */
public class LoginFrame extends javax.swing.JFrame 
{
    /**
     * Creates new form LoginFrame
     */
    public LoginFrame() {
        initComponents();
        initUI();
    }

    private void initUI() 
    {
        this.setLocationRelativeTo(null);
        this.setHomePageFrame(new HomePageFrame(this));
        this.getHomePageFrame().setVisible(false);
        
        this.resetInputs();        
    }
    
    private void setPlaceholder(final JTextField textField, final String placeholder)
    {
        textField.setText(placeholder);
        textField.addFocusListener(new FocusListener()
        {
            @Override
            public void focusGained(FocusEvent fe) 
            {
                if (textField.getText().equals(placeholder)) 
                {
                   textField.setText("");
                }
            }

            @Override
            public void focusLost(FocusEvent fe) 
            {
                if (textField.getText().isEmpty()) 
                {
                   textField.setText(placeholder);
                }  
            }
        }); 
    }
    
    public void resetInputs()
    {
        this.getUsernameInput().setText("");
        this.getPasswordInput().setText("");    
        
        this.setPlaceholder(this.getUsernameInput(), "Enter username");
        this.setPlaceholder(this.getPasswordInput(), "Enter password");
    }
    
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        usernameInput = new javax.swing.JTextField();
        passwordInput = new javax.swing.JPasswordField();
        submitButton = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        usernameInput.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        usernameInput.setToolTipText("username");
        usernameInput.setName(""); // NOI18N

        passwordInput.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        passwordInput.setToolTipText("password");

        submitButton.setText("login");
        submitButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                submitButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(127, 127, 127)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(usernameInput)
                    .addComponent(passwordInput)
                    .addComponent(submitButton, javax.swing.GroupLayout.PREFERRED_SIZE, 143, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(130, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(78, 78, 78)
                .addComponent(usernameInput, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(passwordInput, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(submitButton)
                .addContainerGap(88, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void submitButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_submitButtonActionPerformed
        // TODO add your handling code here:
        Login l = new Login();
        boolean isLoginSuccess = l.doLogin(this.getUsernameInput().getText(), this.getPasswordInput().getText());
        if (isLoginSuccess)
        {
            this.goToHomePage();
        }
        else
        {
            this.showLoginErrorPopup();
        } 
    }//GEN-LAST:event_submitButtonActionPerformed

    public void goToHomePage()
    {
        this.setVisible(false);
        this.getHomePageFrame().setVisible(true);
    }
    
    public void showLoginErrorPopup()
    {
        JOptionPane.showMessageDialog(this, "Wrong username or password");
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(LoginFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new LoginFrame().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPasswordField passwordInput;
    private javax.swing.JButton submitButton;
    private javax.swing.JTextField usernameInput;
    // End of variables declaration//GEN-END:variables

    
    private JFrame homePageFrame;
    public JFrame getHomePageFrame()
    {
         return this.homePageFrame;
    }
    
    public void setHomePageFrame(JFrame newFrame)
    {
        this.homePageFrame = newFrame;
    }
    
    public JTextField getUsernameInput()
    {
        return this.usernameInput;
    }
    
    public void setUsernameInput(JTextField newInput)
    {
        this.usernameInput = newInput;
    }
    
    public JPasswordField getPasswordInput()
    {
        return this.passwordInput;
    }
    
    public void setPasswordInput(JPasswordField newInput)
    {
        this.passwordInput = newInput;
    }
}
